QUALOTT
=======

La projet QUALOTT a pour but d'étudier la qualité de service de messageries OTT.

## Description

Ce dépôt présente le code qui a permis de réaliser des tests sur les applications de messagerie [Signal](https://play.google.com/store/apps/details?id=org.thoughtcrime.securesms&hl=fr&gl=US), [Whatsapp](https://play.google.com/store/apps/details?id=com.whatsapp&hl=fr&gl=US) et [Telegram](https://play.google.com/store/apps/details?id=org.telegram.messenger&hl=fr&gl=US).

Dans le cadre de ce projet, des tests ont été réalisés grâce au matériel suivant:
* Un routeur WiFi sous [Open-WRT](https://openwrt.org/)
* Un téléphone Android
* Une machine Linux hébergeant cette API.

Voici une schématisation du setup utilisé:  
![Image décrivant l'installation, un ordinateur portable et un téléphone sont connectés sur le même routeur](images/image.png)


## Installation

```bash
# Récupération du code avec Git
git clone ${GITLAB_URL} qualott
cd qualott

# Création d'un virtualenv et installation des dépendances requises
python3 -m virtualenv .venv
./.venv/bin/pip install poetry

# Installation des dépendances via poetry.
./.venv/bin/poetry install
```


## Connexion au routeur

Pour assurer la connexion au routeur, le multiplexing SSH est utilisé.
La commande suivante doit être effectuée dans un terminal avant de lancer l'API :
```bash
ssh -o ControlMaster=auto -o ControlPersist=180s -o 'User="tcpdump"' -o ControlPath='~/example/ssh' example@routeur
```
Cette dernière permet d'établir une connexion avec le routeur et de la maintenir.

Une fois que cette connexion existe, l'API va utiliser cette connexion pour exécuter les commandes nécessaires (`tcqdisc` / `tcpdump`) comme suit :
```python
with open('./test.pcap','w') as pcapfile:
        command = _run_command(['ssh', '-o' , 'ControlMaster=auto',
                                '-o', 'ControlPersist=180s', '-o',
                                'ControlPath=~/example/ssh', 'example@routeur'] +
                            ['sudo', 'tcpdump', '-i', interface, '-U', '-w', '-'],
                            stdout=pcapfile, stderr=subprocess.PIPE, background=True)
```

```python
command = _run_command(['ssh', '-o' ,'ControlMaster=auto', '-o', 'ControlPersist=180s','-o', 'ControlPath=~/qualott/api-qos/ssh', 'example@routeur'] + ["ls", "/"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
```

## Commandes potentiellement utilisées :

```bash
sudo tc qdisc add dev ${interface} ${username} netem delay ${delay} loss ${loss} rate ${rate}
sudo tc qdisc add dev wlan0 root netem delay 200ms loss 1% rate 100mbit
```

```bash
sudo tc qdisc del dev wlan0 root
```

```bash
sudo tcpdump -i wlan0 -U -w - not port 22
```


## Utilisation

Pour lancer l'API en mode développement :

```bash
./.venv/bin/python3 -m qualott serve
```

## Licence

Ce projet est sous licence MIT. Une copie intégrale du texte
de la licence se trouve dans le fichier [`MIT.txt`](/LICENSES/MIT.txt).
