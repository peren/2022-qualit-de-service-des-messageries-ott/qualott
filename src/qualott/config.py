# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique - PEReN
#
# SPDX-License-Identifier: MIT

import pathlib

from pydantic import BaseSettings, Field

SCRIPT_DIR = pathlib.Path(__file__).parent.resolve()


class Config(BaseSettings):
    modules_dir: str = Field(str(SCRIPT_DIR / "modules"), description="Path to modules directory.")

    # host: str = Field("127.0.0.1", description="Host for the API to listen on.")
    # port: int = Field(9026, description="Port for the API to listen on.")
    class Config:
        env_file = ".env"
        schema_extra = {"description": "Handle the environment variable for the app configuration"}


env_config = Config()
