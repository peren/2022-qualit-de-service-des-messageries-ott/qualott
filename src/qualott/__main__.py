# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique - PEReN
#
# SPDX-License-Identifier: MIT

import argparse
from typing import Dict, Optional

import uvicorn

from . import APP


class UnknownCommandError(Exception):
    pass


def make_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(prog="QUALOTT", description="API pour les tests du projet de QUALOTT")

    subparsers = parser.add_subparsers(dest="cmd")

    serve_parser = subparsers.add_parser("serve", help="Lancer l'API en mode développement")
    serve_parser.add_argument("--host", default="127.0.0.1")
    serve_parser.add_argument("--port", type=int, default=8001)

    return parser


def run_with_args(args: argparse.Namespace) -> Optional[Dict]:
    if args.cmd == "serve":
        uvicorn.run(APP, host=args.host, port=args.port)
    else:
        raise UnknownCommandError(f"Unknown command: {args.cmd}")


if __name__ == "__main__":
    parser = make_parser()
    args = parser.parse_args()
    try:
        run_with_args(args)
    except UnknownCommandError:
        parser.print_help()
