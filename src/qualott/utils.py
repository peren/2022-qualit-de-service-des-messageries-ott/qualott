# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique - PEReN
#
# SPDX-License-Identifier: MIT

import subprocess
import logging


def _run_command(command, stdout=None, stderr=None, background=False):
    """
    Run and log command, wrapper around subprocess.
    """
    logging.info('Running command "%s"...', " ".join(command))
    if background:
        return subprocess.Popen(
            command,
            stdout=stdout,
            stderr=stderr,
        )

    return subprocess.run(
        command,
        stdout=stdout,
        stderr=stderr,
    )
