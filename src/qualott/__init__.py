# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique - PEReN
#
# SPDX-License-Identifier: MIT

__version__ = "0.1"

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from .routes import router
from .network import network


APP = FastAPI()

APP.include_router(router)
APP.include_router(network)


origins = ["*"]
APP.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
