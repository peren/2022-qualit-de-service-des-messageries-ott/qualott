# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique - PEReN
#
# SPDX-License-Identifier: MIT

*** Settings ***
Library		AppiumLibrary

*** Variables ***
${CONVERSATION}	id=com.whatsapp:id/contact_row_container
${CONVERSATION-TEXT-BOX}	id=com.whatsapp:id/entry
${CONVERSATION-SEND-BUTTON}	id=com.whatsapp:id/send
${DELETE-FOR-EVERYONE-BUTTON}	//android.widget.Button[@text="Delete for everyone"]
${SENT-ICON}	//android.widget.ImageView[@content-desc="Sent"]
${DELIVERED-ICON}	//android.widget.ImageView[@content-desc="Delivered"]
${READ-ICON}	//android.widget.ImageView[@content-desc="Read"]
${LAST-MESSAGE}	//android.widget.TextView[@text='abcdefghijklmnopqrstuvwxyz']
${MESSAGE}	abcdefghijklmnopqrstuvwxyz
${TRASH-BUTTON}	//android.widget.ImageButton[@content-desc="Video call"]

*** Keywords ***
delete
	Long Press	${LAST-MESSAGE}
	Click Element	${TRASH-BUTTON}
	Wait Until Page Contains Element	${DELETE-FOR-EVERYONE-BUTTON}
	Click Element	${DELETE-FOR-EVERYONE-BUTTON}

*** Test Cases ***
Open_Whatsapp
	Open Application        http://localhost:4723/wd/hub    platformName=Android    deviceName=emulator-5554        noReset=true    appPackage=com.whatsapp		appActivity=com.whatsapp.HomeActivity   automationName=Uiautomator2    newCommandTimeout=0
Start_Conversation
	Click Element	${CONVERSATION}
	Wait Until Page Contains Element	${CONVERSATION-TEXT-BOX}   
	${count}=   Get Matching XPath Count	${LAST-MESSAGE}
	Run Keyword If   ${count} > 0    delete
	Input Text	${CONVERSATION-TEXT-BOX}	${MESSAGE}
message_sent
	Click Element	${CONVERSATION-SEND-BUTTON}
	# Wait Until Page Contains Element	${SENT-ICON}	timeout=10
message_delivered
    Wait Until Page Contains Element	${DELIVERED-ICON}	timeout=20
delete
	Run Keyword    delete
close
	Quit Application
