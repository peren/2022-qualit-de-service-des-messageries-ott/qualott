# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique - PEReN
#
# SPDX-License-Identifier: MIT

*** Settings ***
Library		AppiumLibrary

*** Variables ***
${CONVERSATION}	id=com.whatsapp:id/contact_row_container
${CONVERSATION-VOICE-BUTTON}	//android.widget.ImageButton[@content-desc="Voice call"]
${END-CALL-BUTTON}	id=com.whatsapp:id/end_call_btn
${RINGING}	//android.widget.TextView[@text="Ringing"]

*** Test Cases ***
open
	Open Application        http://localhost:4723/wd/hub    platformName=Android    deviceName=emulator-5554        noReset=true    appPackage=com.whatsapp		appActivity=com.whatsapp.HomeActivity   automationName=Uiautomator2
conversation
	Click Element	${CONVERSATION}
	Wait Until Page Contains Element	${CONVERSATION-VOICE-BUTTON}
	#Wait Until Page Contains Element	${CONFIRM-CALL-BUTTON}
	#Click Element	${CONFIRM-CALL-BUTTON}
audio
	Click Element	${CONVERSATION-VOICE-BUTTON}
	Wait Until Page Contains Element	${RINGING}
close
	Wait Until Page Contains Element	${END-CALL-BUTTON}
	Click Element	${END-CALL-BUTTON}
	Quit Application
