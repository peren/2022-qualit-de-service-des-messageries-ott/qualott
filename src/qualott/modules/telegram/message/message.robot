# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique - PEReN
#
# SPDX-License-Identifier: MIT

*** Settings ***
Library		AppiumLibrary

*** Variables ***
${CONVERSATION-BOX}	//android.view.ViewGroup[@index=0]
${CHECKBOX}	//android.widget.CheckBox[@index=0]
${DELETE}	//android.widget.TextView[@text="Delete"]
${DELETE-BUTTON}	//android.widget.ImageButton[@content-desc="Delete"]
${LAST-MESSAGE}		//android.view.ViewGroup[contains(@text, "abcdefghijklmnopqrstuvwxyz")]
${LOADED}	//android.widget.TextView[@text="Telegram"]
${MESSAGE}	//android.view.ViewGroup[contains(@text, "abcdefghijklmnopqrstuvwxyz")]
${MESSAGE-BOX}	//android.widget.EditText[@text="Message"]
${SEEN-ICON}	//android.view.ViewGroup[contains(@text, "seen")]
${SEND-BUTTON}	//android.view.View[@content-desc="Send"]
${SENDING-ICON}	//android.view.ViewGroup[contains(@text, "Sent")]


*** Keywords ***
delete
	Long Press	${LAST-MESSAGE}
	Wait Until Page Contains Element        ${DELETE-BUTTON}	timeout=10
	Click Element	${DELETE-BUTTON}
	Wait Until Page Contains Element        ${DELETE}	timeout=10
    Click Element At Coordinates        70     1250
	# Click Element	${CHECKBOX}
    Click Element    ${DELETE}

*** Test Cases ***
open
	Open Application	http://localhost:4723/wd/hub	platformName=Android	deviceName=emulator-5554	noReset=true	appPackage=org.telegram.messenger	appActivity=org.telegram.ui.LaunchActivity	automationName=Uiautomator2    newCommandTimeout=0
	Wait Until Page Contains Element	${CONVERSATION-BOX}	timeout=10
	Wait Until Page Contains Element	${LOADED}	timeout=10
conversation
	Click Element	${CONVERSATION-BOX}
	Wait Until Page Contains Element	${MESSAGE-BOX}	timeout=10
	${count}=   Get Matching XPath Count	${LAST-MESSAGE}
	Run Keyword If   ${count} > 0    delete
	Input Text	${MESSAGE-BOX}	abcdefghijklmnopqrstuvwxyz
	Wait Until Page Contains Element	${SEND-BUTTON}	timeout=10
message_sent
	Click Element	${SEND-BUTTON}
	Wait Until Page Contains Element	${SENDING-ICON}	timeout=10
message_delivered
	Wait Until Page Contains Element	${SEEN-ICON}	timeout=10
delete
	Run Keyword 	delete
close
    Quit Application
