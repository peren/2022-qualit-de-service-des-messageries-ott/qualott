# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique - PEReN
#
# SPDX-License-Identifier: MIT

*** Settings ***
Library		AppiumLibrary	timeout=15

*** Variables ***
${CONVERSATION-BOX}	//android.view.ViewGroup[@index=0]
${LOADED}	//android.widget.TextView[@text="Telegram"]
${MESSAGE-BOX}	//android.widget.EditText[@text="Message"]
${SEND-BUTTON}	//android.view.View[@content-desc="Send"]
${SENDING-ICON}	//android.view.ViewGroup[contains(@content-desc, "Sent") and contains(@content-desc, "Hello Telegram") and (@bounds="[0,1891][1080,1977]")]
${DROP-DOWN-BUTTON}	//android.widget.ImageButton[@content-desc="Plus d'options"]
${END-CALL-BUTTON}	//android.widget.Button[@text="End call"]
${VOICE-CALL-BUTTON}    //android.widget.ImageButton[@bounds="[816,136][948,290]"]
${LOADED-CALL}	//android.widget.TextView[contains(@text, "Ringing")]

*** Test Cases ***
open
	Open Application	http://localhost:4723/wd/hub	platformName=Android	deviceName=emulator-5554	noReset=true	appPackage=org.telegram.messenger	appActivity=org.telegram.ui.LaunchActivity	automationName=Uiautomator2
	Wait Until Page Contains Element	${CONVERSATION-BOX}	
	Wait Until Page Contains Element	${LOADED}
conversation
	Click Element	${CONVERSATION-BOX}
	Wait Until Page Contains Element        ${MESSAGE-BOX}
        Wait Until Page Contains Element        ${VOICE-CALL-BUTTON}
audio
        Click Element   ${VOICE-CALL-BUTTON}
	Wait Until Page Contains Element	${LOADED-CALL}
close
	Wait Until Page Contains Element        ${END-CALL-BUTTON}
        Click Element   ${END-CALL-BUTTON}
        Quit Application
