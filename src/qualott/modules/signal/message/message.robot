# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique - PEReN
#
# SPDX-License-Identifier: MIT

*** Settings ***
Library		AppiumLibrary
# Test Timeout    2 minutes

*** Variables ***
${CONTACT-BOX}      id=org.thoughtcrime.securesms:id/conversation_list_item_name
${NEW-MESSAGE-BOX}      id=org.thoughtcrime.securesms:id/message
${MESSAGE-BOX}      id=org.thoughtcrime.securesms:id/embedded_text_editor
${MESSAGE}	abcdefghijklmnopqrstuvwxyz
${SEND-BUTTON}      id=org.thoughtcrime.securesms:id/send_button
${SENT-MESSAGE-INDICATOR}       //android.widget.ImageView[@resource-id='org.thoughtcrime.securesms:id/sent_indicator' or @resource-id='org.thoughtcrime.securesms:id/delivered_indicator']
${DELIVERED-MESSAGE-INDICATOR}       id=org.thoughtcrime.securesms:id/delivered_indicator
${LAST-MESSAGE}		//android.widget.TextView[@text='abcdefghijklmnopqrstuvwxyz']
${DELETE-BUTTON}     //*[contains(@text,"Delete")]
${REACTION-LIKE}    id=org.thoughtcrime.securesms:id/reaction_1
${CONFIRMATION-DELETE-BUTTON}	id=android:id/button3

*** Keywords ***
delete
    Long Press  ${LAST-MESSAGE}
    Wait Until Page Contains Element    ${REACTION-LIKE}	timeout=5
   Click Element At Coordinates        800     2159
    # Click Element    ${DELETE-BUTTON}
    Wait Until Page Contains Element    ${CONFIRMATION-DELETE-BUTTON}	timeout=10
    Click Element   ${CONFIRMATION-DELETE-BUTTON}

*** Test Cases ***
Open_Signal
    Open Application        http://localhost:4723/wd/hub    platformName=Android    deviceName=emulator-5554        noReset=true    appPackage=org.thoughtcrime.securesms       appActivity=org.thoughtcrime.securesms.ShortcutLauncherActivity automation=Uiautomator2    newCommandTimeout=0
    Wait Until Page Contains Element        ${CONTACT-BOX}	timeout=5
Start_Conversation
    Click Element   ${CONTACT-BOX}
    Wait Until Page Contains Element    ${MESSAGE-BOX}	timeout=5
    ${count}=   Get Matching XPath Count        ${LAST-MESSAGE}
    Run Keyword If   ${count} > 0    delete
    Input Text  ${MESSAGE-BOX}	${MESSAGE}
    Click Element   ${SEND-BUTTON}
message_sent
    Wait Until Page Contains Element    ${SENT-MESSAGE-INDICATOR}	timeout=10
message_delivered
    Wait Until Page Contains Element    ${DELIVERED-MESSAGE-INDICATOR}	timeout=30
delete
    Run Keyword	delete
Close_Signal
	Quit Application


