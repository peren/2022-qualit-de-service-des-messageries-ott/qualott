# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique - PEReN
#
# SPDX-License-Identifier: MIT

***Settings ***
Library		AppiumLibrary

*** Variables ***
${CONTACT-BOX}      id=org.thoughtcrime.securesms:id/conversation_list_item_name
${NEW-CALL-BOX}      id=org.thoughtcrime.securesms:id/menu_call_secure
${CONFIRMATION-CALL-BUTTON}     id=android:id/button1
${CALL-STATUS-RINGING}      //android.widget.TextView[starts-with(@text, 'Ring')]
${HANG-UP-BUTTON}       id=org.thoughtcrime.securesms:id/call_screen_end_call
${LAST-CALL-BOX}        id=org.thoughtcrime.securesms:id/conversation_update_item   #Does not get the last but the first ...
${DELETE-BUTTON}    //android.widget.ImageView[@content-desc='More options']
${CONFIRMATION-DELETE-BUTTON}     //android.widget.Button[starts-with(@text, 'DELETE FOR')]
*** Test Cases ***
Open_Signal
    Open Application        http://localhost:4723/wd/hub    platformName=Android    deviceName=emulator-5554        noReset=true    appPackage=org.thoughtcrime.securesms       appActivity=org.thoughtcrime.securesms.ShortcutLauncherActivity automation=Uiautomator2
    Wait Until Page Contains Element        ${CONTACT-BOX}
Start_Conversation
    Click Element   ${CONTACT-BOX}
    Wait Until Page Contains Element    ${NEW-CALL-BOX}
    Click Element   ${NEW-CALL-BOX}
    Wait Until Page Contains Element    ${CONFIRMATION-CALL-BUTTON}
audio
    Click Element   ${CONFIRMATION-CALL-BUTTON}
    Wait Until Page Contains Element   ${CALL-STATUS-RINGING}  20s
Hang_Up    
    Click Element   ${HANG-UP-BUTTON}
#Delete_call
#    Wait Until Page Contains Element   ${LAST-CALL-BOX}
#    Long Press      ${LAST-CALL-BOX}
#    Click Element       ${DELETE-BUTTON}
#    Wait Until Page Contains Element    ${CONFIRMATION-DELETE-BUTTON}
#    Click Element   ${CONFIRMATION-DELETE-BUTTON}
#    Sleep       0.2s
Close_Signal
	Quit Application
