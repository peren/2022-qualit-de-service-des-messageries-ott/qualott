# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique - PEReN
#
# SPDX-License-Identifier: MIT

import subprocess

from fastapi import APIRouter, HTTPException

from .config import env_config
from .utils import _run_command


network = APIRouter()

port = 22


@network.on_event("startup")
async def startup_event():
    # Check if a ssh connection is still running
    command = _run_command(
        [
            "ssh",
            "-o",
            "ControlMaster=auto",
            "-o",
            "ControlPersist=180s",
            "-o",
            "ControlPath=~/qualott/ssh",
            "example@routeur",
            "ls",
            "/",
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    try:
        command.check_returncode()
    except Exception:
        raise HTTPException(status_code=400, detail="No SSH Connection alive")


@network.get("/network_start")
def start_recording(module: str, feature: str, interface: str = "wlan0", filename: str = "latest_capture.pcap"):
    with open(f"{env_config.modules_dir}/{module}/{feature}/{filename}", "w") as pcapfile:
        _run_command(
            [
                "ssh",
                "-o",
                "ControlMaster=auto",
                "-o",
                "ControlPersist=180s",
                "-o",
                "ControlPath=~/qualott/ssh",
                "example@routeur",
            ]
            + ["sudo", "tcpdump", "-i", interface, "-U", "-w", "-"],
            stdout=pcapfile,
            stderr=subprocess.PIPE,
            background=True,
        )
    return None


@network.get("/network_stop")
def stop_recording():
    print("killing tcpdump process")
    _run_command(
        [
            "ssh",
            "-o",
            "ControlMaster=auto",
            "-o",
            "ControlPersist=180s",
            "-o",
            "ControlPath=~/qualott/ssh",
            "example@routeur",
        ]
        + ["sudo", "/usr/bin/killall", "tcpdump"],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        background=True,
    )
    return None


@network.get("/network")
def set_network(interface: str = "wlan0", delay: str = "20ms", loss: str = None, rate: str = None):
    command = _run_command(
        [
            "ssh",
            "-o",
            "ControlMaster=auto",
            "-o",
            "ControlPersist=180s",
            "-o",
            "ControlPath=~/qualott/ssh",
            "example@routeur",
        ]
        + ["sudo", "tc", "qdisc", "add", "dev", interface, "root", "netem", "delay", delay]
        + (["loss", loss] if loss else [])
        + (["rate", rate] if rate else []),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        background=True,
    )
    return command.stdout


@network.delete("/network")
def reset_network(interface: str = "wlan0"):
    command = _run_command(
        [
            "ssh",
            "-o",
            "ControlMaster=auto",
            "-o",
            "ControlPersist=180s",
            "-o",
            "ControlPath=~/qualott/ssh",
            "example@routeur",
        ]
        + ["sudo", "tc", "qdisc", "del", "dev", interface, "root"],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        background=True,
    )
    return command.stdout
