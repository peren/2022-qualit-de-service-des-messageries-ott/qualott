# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique - PEReN
#
# SPDX-License-Identifier: MIT

import requests
import robot

from dateutil.parser import parse
from fastapi import APIRouter, HTTPException
from fastapi.responses import RedirectResponse
from robot.api import ExecutionResult
from scapy.utils import rdpcap

from .config import env_config


router = APIRouter()


@router.get("/", include_in_schema=True)
async def redirect_to_docs():
    """
    Redirect to /docs page.
    """
    return RedirectResponse("./docs")


@router.get("/{module}/{feature}")
def run_test(module: str, feature: str):
    requests.get(f"http://localhost:8001/network_start?module={module}&feature={feature}")
    robot.run(
        f"{env_config.modules_dir}/{module}/{feature}/{feature}.robot",
        outputdir=f"{env_config.modules_dir}/{module}/{feature}",
        exitonfailure=True,
    )
    list_result = []
    try:
        for test in ExecutionResult(f"{env_config.modules_dir}/{module}/{feature}/output.xml").suite.tests:
            if feature in test.name:
                list_result.append(
                    {
                        "name": test.name,
                        "status": test.status,
                        "elapsedtime": test.elapsedtime,
                        "starttime": test.starttime,
                        "endtime": test.endtime,
                    }
                )
    except Exception:
        raise HTTPException(status_code=404, detail="Error reading output.xml")
    requests.get("http://localhost:8001/network_stop")
    pcap_path = f"{env_config.modules_dir}/{module}/{feature}/latest_capture.pcap"
    try:
        scapy_cap = rdpcap(pcap_path)
    except Exception:
        raise HTTPException(status_code=404, detail="Error reading pcap file")
    start_time = parse(list_result[0]["starttime"]).timestamp()
    end_time = parse(list_result[-1]["endtime"]).timestamp()
    print(start_time)
    print(end_time)
    somme = 0
    for packet in scapy_cap:
        if packet.time <= end_time and packet.time >= start_time:
            somme += len(packet)
    list_result.append({"data": somme})
    return list_result
